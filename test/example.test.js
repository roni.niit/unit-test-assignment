//test/example.test.js

const expect = require('chai').expect;
const should = require('chai').should();
const { assert } = require('chai');
const mylib = require('../src/mylib');

describe('Unit testing mylib.js', () => {
    let myvar = undefined;
   // it('Should return 2 when using sum function with a=1, b=1', () => {
     //   const result = mylib.sum(1,1);
     //   expect(result).to.equal(2);
    //})

    before(() => {
        myvar = 1;
        console.log('Before testing');
    });

    it('Should return 2 when using sum function with a=1, b=1', () => {
        const result = mylib.sum(1,1);
        expect(result).to.equal(2);
    });

    it('Should return 3 when using subtraction function with a=5, b=2', () => {
        const result = mylib.subtract(5,2);
        expect(result).to.equal(3);
    });

    it('Assert foo is not bar', () => {
        assert('foo' !== 'bar')
    })

    after(() => console.log('After mylib test'));


    

});